## What's this?
A simple script that I will use to reinstall my Fedora system.

## Required ansible packages for this script to work:
`ansible` - duh

`ansible-collection-ansible-posix` - for mounting filesystems

`ansible-collection-community-general` - for managing flatpacks

## Does it work?
It should now!

## What about borg's repository configuration?
Currently it is written with borg's repositories created with `-e keyfile` and with no password. I actually have repository created with `-e repokey`, "support" should be added soon.
